<?php

global $user;
define('CURRENT_USER', $user->name);

/**
 * Implementation of hook_field_info().
 * defines fields the module provides
 */
function droodle_field_info() {
  return array(
    // field droodle
    'droodle' => array(
      'label' => 'Droodle',
      'description' => t('Stores the id and title of Droodles'),
      'default_widget' => 'droodle_widget_default',
      'default_formatter' => 'droodle_formatter_default',
    ),
      // more fields would go here
  );
}

/**
 * Implementation of hook_field_settings_form().
 */
function droodle_field_settings_form($field, $instance) {
  
}

/**
 * Implementation of hook_field_widget_info().
 * defines a widget. A widget is ususally an input form (for example on edit page) that receives input for the field data.
 */
function droodle_field_widget_info() {
  return array(
    // the name of the default widget is 'droodle_widget_default'
    'droodle_widget_default' => array(
      'label' => t('Droodle widget'),
      // the field types this widget can process
      'field types' => array('droodle'), // in our case only the droodle field
      'module' => ''
    ),
  );
}

/**
 * Implementation of hook_field_widget_form().
 * this function creates the input form of the widget.
 */
function droodle_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $n_did = 'droodle_id';
  $n_dtitle = 'droodle_title';
  $parents = $form['#parents'];
  $field_name = $field['field_name'];
  $item = array(
    $n_did => '0',
    $n_dtitle => 'Droodle?',
  );
  if (!empty($items[$delta])) {
    $item = $items[$delta];
  }
  //always recover items from form_state 
  if (isset($form_state['values'])) {
    $key_exists = NULL;
    $deltaParent = array_merge($parents, array($field_name, $langcode, $delta));
    $value = drupal_array_get_nested_value($form_state['values'], $deltaParent, $key_exists);
    if ($key_exists) {
      $item = $value; // replace item with value from $form_state
    }
  }
  $default_id = $item[$n_did];
  $default_title = $item[$n_dtitle];

  $element[$n_did] = array(
    '#type' => 'select',
    '#title' => t('Id'),
    '#size' => 5,
    //'#default_value' => $default_id,
    '#options' => array(0, 1, 2, 3),
  );
  $element[$n_dtitle] = array(
    '#type' => 'textfield',
    '#title' => t('Title / Question'),
    '#size' => 60, // match db field size
    '#default_value' => $default_title,
  );
  return $element;
}

/**
 * Implementation of hook_field_validate().
 */
function droodle_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // TODO validate input here?
  foreach ($items as $delta => $item) {
    if (!empty($item['droodle_title'])) {
      if (!check_plain($item['droodle_title'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'droodle_invalid',
          'message' => t('Input musst be plain text'),
        );
      }
    }
  }
}

/**
 * Implementation of hook_field_is_empty().
 */
function droodle_field_is_empty($item, $field) {
  $empty = empty($item['droodle_title']) || $item['droodle_title'] == 'Droodle?';
  // test if a droodle field is empty or default
  return $empty;
}

/**
 * Implements hook_field_formater_info().
 */
function droodle_field_formatter_info() {
  return array(
    'droodle_formatter_default' => array(
      'label' => t('Default formatter'),
      'field types' => array('droodle'),
    ),
  );
}

/**
 * Implementation of hook_field_formatter_view().
 */
function droodle_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  if (count($items) === 0) {
    return array();
  }
  switch ($display['type']) {
    case 'droodle_formatter_default':
      $element = array();
      $element[] = drupal_get_form('droodle_exposed_formatter', $items);
      break;
  }
  return $element;
}

/**
 * formates a droodle into a table to be included in a form
 */
function droodle_exposed_formatter($form, &$form_state, $items) {
  foreach ($items as $delta => $item) {
    // foreach droodle build a table
    $d_id = (int) $item['droodle_id'];
    $droodle = new droodle($d_id); // create new droodle object
    $table_name = 'table_' . $d_id;  
    $form[$table_name] = $droodle->get_as_table($form_state);
    $form[$table_name]['droodle_id'] = array(
      '#type' => 'hidden',
      '#value' => $droodle->id,
    );
  }
  return $form;
}

function droodle_exposed_formatter_submit($form, &$form_state) {
  // TODO function droodle_exposed_formatter_submit($form, &$form_state)
  // TODO extract triggering_element
  $triggering_element = $form_state['triggering_element']; 
  $parent_table = $triggering_element['#array_parents'][0];
  
  switch ($triggering_element['#name']) {
    case droodle::VOTE_BUTTON:
      // TODO save new vote
      $values = $form_state['values'];
      debug($values);
      break;
    case droodle::EDIT_BUTTON:
      _edit_vote();
      break;
    default:
      break;
  }
}

function _edit_vote() {
  
}

function _save_vote() {
  
}

function droodle_edit_callback($form, &$form_state) {  
  $triggering_element = $form_state['triggering_element'];
  $parent_table = $triggering_element['#array_parents'][0];  
  $table = $form[$parent_table];
  $row = $table['#rows'][CURRENT_USER]['data'];
  $droodle = new droodle($form_state['values']['droodle_id']);
  foreach($droodle->options as $oid => $option) {
    $row[$oid]['data']['edit']['#prefix'] = '<div>';
  }
  $row['button']['data'][droodle::VOTE_BUTTON]['#prefix'] = '<div>';
  $form_state['rebuild'] = TRUE;
  //dfb($form_state);
  return $table;
}
