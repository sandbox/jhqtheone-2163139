<?php

/**
 * Droodle bundles the data of one poll into an object.
 * It provides utility functions and encapsulates all database actions.
 *
 * @author JHQtheOne
 */
class droodle {

  const VOTE_BUTTON = 'Save';
  const EDIT_BUTTON = 'Edit';

  public $id;
  public $title;
  public $options;
  public $answers;
  public $votes;
  public $current_user;
  public $user_voted;

  /**
   * Constructor, will attempt to load the data from the other data tables
   * @param type $id of the noodle3
   * @param type $title of the noodle3
   */
  function __construct($id) {
    $this->id = (int) $id;
    // set default values 
    $this->title = 'Droodle?';
    $this->options = array();
    $this->answers = array();
    $this->votes = array();
    if (is_numeric($id)) {
      $this->load_answers();
      $this->load_options();
      $this->load_votes();
    }
    global $user;
    $this->current_user = $user->name;
    $this->user_voted = $this->user_voted($this->current_user);
  }

  private function load_answers() {
    // load answers
    $answers_query = db_select('droodle_answer', 'a')
        ->condition('a.droodle_id', $this->id, '=')
        ->fields('a');
    $answers_query_result = $answers_query->execute()->fetchAll(PDO::FETCH_ASSOC);
    foreach ($answers_query_result as $num => $result) {
      $answer = new answer($result['answer_id'], $result['label'], $result['class']);
      $this->answers[$answer->id] = $answer;
    }
  }

  private function load_options() {
    // load options
    $options_query = db_select('droodle_option', 'o')
        ->condition('o.droodle_id', $this->id, '=')
        ->fields('o');
    $options_query_result = $options_query->execute()->fetchAll(PDO::FETCH_ASSOC);
    foreach ($options_query_result as $num => $result) {
      $option = new option((int) $result['option_id'], $result['label'], $this->answers);
      $this->options[$option->id] = $option;
    }
  }

  private function load_votes() {
    // load votes
    $vote_query = db_select('droodle_option', 'o')
        ->condition('o.droodle_id', $this->id, '=');
    $vote_query->join('droodle_vote', 'v', 'o.option_id=v.options_id');
    $vote_query->fields('v');
    $vote_query_result = $vote_query->execute()->fetchAll(PDO::FETCH_ASSOC);
    $votes_array = array();
    // aggregate results by option id
    foreach ($vote_query_result as $num => $result) {
      $user = user_load($result['uid']);
      $user_name = $user->name;
      $oid = (int) $result['options_id'];
      $votes_array[$user_name][$oid] = $this->get_answer($result['answers_id']);
    }
    foreach ($votes_array as $user_name => $values) {
      $this->votes[$user_name] = new vote($user_name, $values);
    }
  }

  /**
   * Checks if a user has already voted fot this noodle
   * @param type $user_id the id of the user to check
   * @return boolean
   */
  public function user_voted($user_name) {
    $user_voted = false;
    foreach ($this->votes as $user_name2 => $vote) {
      if ($user_name == $user_name2) {
        $user_voted = true;
      }
    }
    return $user_voted;
  }

  /**
   * Returns a answer by id
   * @param type $answer_id
   * @return type
   */
  public function get_answer($answer_id) {
    return $this->answers[$answer_id];
  }

  /**
   * Gets the title and the option labels in form of an array that can be used 
   * in a table as header. 
   * @return type array containing the header of the droodle table
   */
  public function get_table_header() {
    $header = array();
    $header[] = $this->title;
    foreach ($this->options as $option) {
      $header[] = $option->label;
    }
    return $header;
  }

  /**
   * Helper function gets the votes cast on this droodle as table rows to be 
   * insterted into a table.
   * @return string
   */
  private function get_table_votes() {
    $table_votes = array();
    foreach ($this->votes as $user_name => $vote) {
      $vote_row = array(
        'data' => array(
          $user_name => array(
            'data' => $user_name,
            'align' => 'right',
          ),
        ),
        'no_striping' => TRUE,
      );
      foreach ($this->options as $oid => $option) {
        if (isset($vote->values[$oid])) {
          /* @var $answer answer */
          $answer = $vote->values[$oid];
          $vote_row['data'][$oid] = array(
            'data' => array(
              array(
                '#type' => 'markup',
                '#markup' => $answer->label,
              ),
            ),
            //'align' => 'center',
            'class' => $answer->class,
          );
        } else {
          $vote_row['data'][$oid] = array(
            'data' => '&nbsp;',
          );
        }
      }
      $table_votes[$user_name] = $vote_row;
    }
    return $table_votes;
  }

  /**
   * Helper function inserts input form elements into the given table
   * @param type $table
   */
  public function insert_table_input_row(&$table, $row_name = 'input_row') {
    $table['#rows'][$row_name]['data'][$this->current_user] = array(
      'data' => $this->current_user,
      'align' => 'right',
    );
    $table['#rows'][$row_name]['no_striping'] = TRUE;
    foreach ($this->options as $oid => $option) {
      $radios = array();
      foreach ($option->answers as $aid => $answer) {
        $radios[$answer->id] = $answer->label;
      }
      $input_elements = array(
        '#type' => 'radios',
        '#id' => 'droodle_' . $this->id . '_rg_' . $oid,
        '#options' => &$radios,
      );
      $table['#rows'][$row_name]['data'][$oid] = array(
        'data' => &$input_elements,
      );
      $table[$oid] = &$input_elements;
      unset($input_elements);
      unset($radios);
    }
    $vote_button = $this->get_table_vote_button();
    $table['#rows'][$row_name]['data']['button'] = array(
      'data' => &$vote_button,
      'style' => 'border: 0px',
    );
    $table[droodle::VOTE_BUTTON] = &$vote_button;
  }

  /**
   * Creating form element vote_button
   * @return type
   */
  public function get_table_vote_button() {
    $vote_button = array(
      '#type' => 'submit',
      '#name' => droodle::VOTE_BUTTON,
      '#value' => t(droodle::VOTE_BUTTON),
    );
    return $vote_button;
  }

  /**
   * Creating form element edit_button
   * @return type
   */
  public function get_table_edit_button() {
    $edit_button = array(
      '#type' => 'submit',
      '#name' => droodle::EDIT_BUTTON,
      '#value' => t(droodle::EDIT_BUTTON),
      '#ajax' => array(
        'wrapper' => $this->get_table_name(),
        'callback' => 'droodle_edit_callback',
      ),
    );
    return $edit_button;
  }

  /**
   * Include an edit button to the row of the current user
   * @param type $table the table refenrence to add the edit button to
   */
  private function insert_table_edit_button(&$table) {
    $edit_button = $this->get_table_edit_button();
    foreach ($table['#rows'] as $user_name => $row) {
      if ($user_name == $this->current_user) {
        $edit_button = $this->get_table_edit_button();
        $table['#rows'][$user_name]['data']['button'] = array(
          'data' => array(&$edit_button),
          'style' => 'border: 0px',
        );
        $table[droodle::EDIT_BUTTON] = &$edit_button;
        
        $vote_button = $this->get_table_vote_button();
        $vote_button['#prefix'] = '<div style="display: none">';
        $vote_button['#suffix'] = '</div>';
        $table['#rows'][$user_name]['data']['button']['data'][droodle::VOTE_BUTTON] = &$vote_button;
        $table[droodle::VOTE_BUTTON] = &$vote_button;

        foreach ($this->options as $oid => $option) {
          $radios = array();
          foreach ($option->answers as $aid => $answer) {
            $radios[$answer->id] = $answer->label;
          }
          $input_elements = array(
            '#type' => 'radios',
            '#id' => 'droodle_' . $this->id . '_rg_' . $oid . '_edit',
            '#options' => &$radios,
            // TODO '#default_value' => ,
            '#prefix' => '<div style="display: none;">',
            '#suffix' => '</div>'
          );
          $table['#rows'][$user_name]['data'][$oid]['data']['edit'] = &$input_elements;
          $table[$oid . '_edit'] = &$input_elements;
          unset($input_elements);
          unset($radios);
        }
      }
    }
  }

  /**
   * Helperfunction generates a unique table name 
   */
  private function get_table_name() {
    return 'droodle_table_' . $this->id;
  }

  /**
   * Creating an output for this droodle that will be themed as a table and 
   * contains form elements to save a new vote or edit a previously cast vote.
   * 
   * Very important here: 
   * The form elements are added twice and by reference! 
   * Once for the themeing function into $table['#rows'],
   * and again into the $table directely for the form_builder function.
   * each of the functions ignores the other reference, so there will be only 
   * one refenrence added.
   * 
   * Thanks to Thomas Sutton: 
   * http://passingcuriosity.com/2011/drupal-7-forms-tables/
   * 
   * @return string
   */
  public function get_as_table($form_state) {
    $table_name = $this->get_table_name();
    $table = array(
      '#id' => $table_name,
      '#name' => $table_name,
      //'#caption' => $table_name,
      '#sticky' => false,
      '#theme' => 'table',
      '#header' => $this->get_table_header(),
      '#rows' => $this->get_table_votes(),
      '#attributes' => array(
        'id' => $table_name,
      ),
    );
    if ($this->user_voted) {
      // user has allready voted - insert edit_button      
      $this->insert_table_edit_button($table);
    } else {
      // user has yet to vote - insert input row with vote_button
      $this->insert_table_input_row($table, $this->current_user);
    }
    dfb($table['#rows'][CURRENT_USER]['data']);
    return $table;
  }

}

class answer {

  public $id;
  public $label;
  public $class;

  function __construct($answer_id, $label, $class) {
    $this->id = $answer_id;
    $this->label = $label;
    $this->class = $class;
  }

}

class option {

  public $id;
  public $label;
  public $answers;

  function __construct($option_id, $label, $answers) {
    $this->id = $option_id;
    $this->label = $label;
    $this->answers = $answers;
  }

}

class vote {

  public $user_name;
  public $values; // option_id => answer_id

  function __construct($user_name, $values) {
    $this->user_name = $user_name;
    $this->values = $values;
  }

}
